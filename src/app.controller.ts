import { Controller, Get } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { InjectModel } from '@nestjs/mongoose';
import { json } from 'express';
import { Model } from 'mongoose';
import { stringify } from 'querystring';
import { XmlUtils } from 'src/xml.utils';
import * as soap from 'soap';

@Controller()
export class AppController {
  XmlUtils:XmlUtils;
  url:string = 'http://mysoap.wsdl';
args = { name: 'value' };

  constructor(  @InjectModel('incident') private readonly incidentModel: Model<any>,) { }

  @EventPattern( "super" )
  async handleMessagePrinted(data: Record<any, any>) {
    let dataXML;
    console.log(data);   
 
  this.insertincident(data); 

    const client = await soap.createClientAsync(this.url);
    const result = client.MyFunctionAsync(this.args);
    console.log(result);

  }

  async insertincident(data:any) {
    stringify(data);
    this.incidentModel.insertMany(data);

}


}

