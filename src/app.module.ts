import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';import { MongooseModule } from '@nestjs/mongoose';
import { incidentSchema,incident } from './incident/incident.model';


@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost/nest'),MongooseModule.forFeature([{ name: "incident", schema: incidentSchema }])],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}


