import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';import { logConfig } from './config/logger/log.config';
import { Transport } from '@nestjs/microservices';
import { AllExceptionFilter } from './common/exceptions/all-exception.filter';
import { LoggingInterceptor } from './common/interceptors/logging.interceptor';

async function bootstrap() {


  // Create APP + Logger
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: "amqps://smith:LyRhpZ-OxT7c5@grouse.rmq.cloudamqp.com/towels",
      queue: "towels",
      queueOptions: {
        durable: true,
      },
    },
  });

  await app.listen(3000);
}

bootstrap();

