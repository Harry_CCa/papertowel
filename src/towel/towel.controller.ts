import { Controller, ValidationPipe, UsePipes, Body, Post } from '@nestjs/common';

import { resolve } from 'path';
import { towelService } from './towel.service';


@Controller('towel')
export class towelController {
  constructor(private towelService: towelService) {}

  @Post('start')
  async start(@Body() towel: any): Promise<any> {
 
     await this.towelService.Save(towel);

     
      return towel;
  
  }
}
