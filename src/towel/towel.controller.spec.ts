import { Test, TestingModule } from '@nestjs/testing';
import { towelController } from './towel.controller';

describe('towelController', () => {
  let controller: towelController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [towelController],
    }).compile();

    controller = module.get<towelController>(towelController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
