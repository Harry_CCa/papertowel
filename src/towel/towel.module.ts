import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { towelController } from './towel.controller';
import { towelService } from './towel.service';
@Module({
  imports: [
    ConfigModule,
    MongooseModule.forFeature([
    ]),
  ],
  controllers: [towelController],
  providers: [towelService],
  exports: [towelService],
})
export class towelModule {}
