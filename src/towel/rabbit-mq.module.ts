import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { TowelController } from './towel.controller';
import { TowelService } from './towel.service';
@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'rabbit-mq-module',
        transport: Transport.RMQ,
        options: {
          urls: [
            'amqps://usermc:password@grouse.rmq.cloudamqp.com/xiuserc',
          ],
          queue: 'queue',
        },
      },
    ]),
  ],
  controllers: [],
  providers: [],
  exports: [RabbitMQModule],
})
export class RabbitMQModule {}